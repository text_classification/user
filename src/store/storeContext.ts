import React from 'react';
import { IStoreAction } from './storeActions';
import { IStore } from './storeReducer';

const storeContext = React.createContext<{
    store: IStore;
    dispatchStore: React.Dispatch<IStoreAction>;
}>({} as any);
export default storeContext;
