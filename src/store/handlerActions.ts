import { IStore } from './storeReducer';

namespace handlerActions {
    export function userName(preState: IStore, payload: string) {
        return {
            ...preState,
            userName: payload,
        } as IStore;
    }
}

export default handlerActions;
