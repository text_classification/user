import * as React from 'react';
import storeContext from './storeContext';
import storeReducer, { initStore } from './storeReducer';

export interface IProviderProps {}

export default function Provider(props: React.PropsWithChildren<IProviderProps>) {
    const [store, dispatchStore] = React.useReducer(storeReducer, initStore);

    return (
        <storeContext.Provider value={{ store, dispatchStore }}>
            {props.children}
        </storeContext.Provider>
    );
}
