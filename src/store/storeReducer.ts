import handlerActions from './handlerActions';
import { EActions, IStoreAction } from './storeActions';

export interface IStore {
    userName?: string;
}

export const initStore: IStore = {
    userName: 'UserName',
};

export default function storeReducer(preState: IStore, action: IStoreAction) {
    switch (action.type) {
        case EActions.userName: {
            return handlerActions.userName(preState, action.payload);
        }
    }
}
