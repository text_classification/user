export enum EActions {
    userName = 'userName',
}

export interface IStoreAction {
    type: EActions;
    payload: any;
}

namespace storeAction {
    export function setUserName(v: string) {
        return {
            type: EActions.userName,
            payload: v,
        } as IStoreAction;
    }
}

export default storeAction;
