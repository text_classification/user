import axios from 'axios';
import ENV from '../core/ENV';

export interface IPredictResponse {
    result: string;
}

async function apiPredict(v: string) {
    const url = `${ENV.API_HOST}/predict/${v}`;
    // const url = 'http://tuannguyencoder.sytes.net:5555/predict/hajksdaskd';
    try {
        const res = await axios.get(url);
        if (res.status !== 200) return undefined;

        const resData: IPredictResponse = res.data;
        return resData;
    } catch {
        return undefined;
    }
}

export default apiPredict;
