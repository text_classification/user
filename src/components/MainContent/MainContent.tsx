import * as React from 'react';
import Comments from '../Comments';
import Container from '../Container';

import './MainContent.scss';

export interface IMainContentProps {}

export default function MainContent(props: IMainContentProps) {
    return (
        <div className='main-contain'>
            <Container>
                <Comments />
            </Container>
        </div>
    );
}
