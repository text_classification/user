import * as React from 'react';
import storeAction from '../../store/storeActions';
import storeContext from '../../store/storeContext';
import Container from '../Container';

import './Header.scss';

export interface IHeaderProps {}

export default function Header(props: IHeaderProps) {
    const { store, dispatchStore } = React.useContext(storeContext);

    const handlerInputChange = (e: React.FormEvent<HTMLInputElement>) => {
        const v = e.currentTarget.value;
        dispatchStore(storeAction.setUserName(v));
    };

    return (
        <header>
            <Container>
                <h1>Bài toán phân loại comment</h1>
                <input
                    type='text'
                    autoComplete='off'
                    onChange={handlerInputChange}
                    value={store.userName}
                />
            </Container>
        </header>
    );
}
