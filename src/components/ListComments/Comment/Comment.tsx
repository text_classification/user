import * as React from 'react';

import './Comment.scss';

export interface ICommentProps {
    avatar?: string;
    createdAt?: string;
    userName?: string;
    content?: string;
    result?: string;
}

export default function Comment(props: ICommentProps) {
    const { avatar, createdAt, userName, content, result } = props;

    return (
        <div className='comment'>
            <div className='avatar' style={{ backgroundImage: `url("${avatar}")` }} />
            <div className='wrap'>
                <div className='userName'>
                    <strong>{userName}</strong>
                    <span>{createdAt}</span>
                </div>
                <div className='content'>{content}</div>
                <div className='result'>{result}</div>
            </div>
        </div>
    );
}
