import * as React from 'react';
import Comment from './Comment';

import './ListComments.scss';

export interface IComment {
    avatar: string;
    userName: string;
    content: string;
    createdAt: string;
    result: string;
}

export interface IListCommentsProps {
    comments?: IComment[];
}

export default function ListComments(props: IListCommentsProps) {
    const { comments } = props;

    const renderElmnts = () => {
        if (!comments || comments.length === 0) return undefined;
        return comments.map((elmnt, i) => {
            return (
                <Comment
                    key={i}
                    avatar={elmnt.avatar}
                    content={elmnt.content}
                    createdAt={elmnt.createdAt}
                    userName={elmnt.userName}
                    result={elmnt.result}
                />
            );
        });
    };

    return <div className='list-comments'>{renderElmnts()}</div>;
}
