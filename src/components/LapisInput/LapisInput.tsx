import * as React from 'react';

import './LapisInput.scss';

export interface ILapisInputProps {
    onEnter?: (v: string) => any;
}

export default function LapisInput(props: ILapisInputProps) {
    const { onEnter } = props;

    const handlerKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        switch (e.key) {
            case 'Enter': {
                const v: string = e.currentTarget.value;
                if (onEnter) onEnter(v);
                e.currentTarget.value = '';
                break;
            }
            default: {
            }
        }
    };

    return (
        <div className='lapis-input'>
            <div className='wrap'>
                <input type='text' autoComplete='off' onKeyDown={handlerKeyDown} />
                <div className='underline'></div>
            </div>
        </div>
    );
}
