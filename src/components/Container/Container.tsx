import * as React from 'react';

import './Container.scss';

export interface IContainerProps {
    className?: string;
}

export default function Container(props: React.PropsWithChildren<IContainerProps>) {
    return <div className={`container ${props.className}`}>{props.children}</div>;
}
