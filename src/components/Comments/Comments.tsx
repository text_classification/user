import * as React from 'react';
import apiPredict from '../../api/apiPredict';
import storeContext from '../../store/storeContext';
import LapisInput from '../LapisInput';
import ListComments from '../ListComments';
import { IComment } from '../ListComments/ListComments';
import './Comments.scss';

export interface ICommentsProps {}

export default function Comments(props: ICommentsProps) {
    const [comments, setComments] = React.useState<IComment[]>([]);
    const { store } = React.useContext(storeContext);

    const handlerInputEnter = async (v: string) => {
        const res = await apiPredict(v);
        if (!res) {
            alert('Có lỗi trong quá trình POST');
            return;
        }

        setComments((preState) => {
            return [
                {
                    avatar: '/images/avatar.jpg',
                    content: v,
                    createdAt: new Date().toJSON(),
                    userName: store.userName || 'UserName',
                    result: res.result,
                },
                ...preState,
            ];
        });
    };

    return (
        <div className='comments'>
            <LapisInput onEnter={handlerInputEnter} />
            <ListComments comments={comments} />
        </div>
    );
}
